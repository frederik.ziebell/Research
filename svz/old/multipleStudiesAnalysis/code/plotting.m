(* ::Package:: *)

Needs["ErrorBarPlots`"]
(* define error bars [code take from Mathematica manual] *)
errorBar[type_:"Rectangle"][{{x0_,x1_},{y0_,y1_}},value_,meta_]:=Block[{error},
																	error=Flatten[meta];
																	error=If[error==={},0,Last[error]];
																	{ChartElementData[type][{{x0,x1},{y0,y1}},value,meta],{
																	Black,Line[{{{(x0+x1)/2,y1-error},{(x0+x1)/2,y1+error}},{{1/4 (3 x0+x1),y1+error},
																	{1/4 (x0+3 x1),y1+error}},{{1/4 (3 x0+x1),y1-error},{1/4 (x0+3 x1),y1-error}}}]}}
																];
(* plot data vs model with both having error bars 
 - data = {Subscript[d, 1]\[Rule]Subscript[e, 1],...,Subscript[d, n]\[Rule]Subscript[e, n]} is the original data with associated error bar heights Subscript[e, i] 
 - model = {Subscript[m, 1]\[Rule]Subscript[f, 1],Subscript[m, n]\[Rule]Subscript[f, n]} is the model prediction with associated error bar heights Subscript[f, i]
 - dataLabels, modelLabels contain what should be displayed underneath the bars
*)
is=300;
plotDvM[data_,model_,dataLabels_,modelLabels_,title_:""]:=BarChart[
															Riffle[data,model],ChartElementFunction->errorBar[],
															ChartLabels->Riffle[dataLabels,modelLabels],
															ImageSize->is,PlotLabel->title
														];
