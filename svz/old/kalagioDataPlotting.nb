(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 9.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     17035,        430]
NotebookOptionsPosition[     16567,        410]
NotebookOutlinePosition[     16911,        425]
CellTagsIndexPosition[     16868,        422]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[{
 RowBox[{
  RowBox[{"SetDirectory", "[", 
   RowBox[{"NotebookDirectory", "[", "]"}], "]"}], ";"}], "\n", 
 RowBox[{
  RowBox[{"Import", "[", "\"\<kalagio.m\>\"", "]"}], ";"}], "\n", 
 RowBox[{
  RowBox[{"is", "=", "300"}], ";"}], "\n", 
 RowBox[{
  RowBox[{
   RowBox[{"plotD", "[", 
    RowBox[{"data_", ",", "dataLabels_", ",", 
     RowBox[{"title_:", "\"\<\>\""}]}], "]"}], ":=", 
   RowBox[{"BarChart", "[", "\n", "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t", 
    RowBox[{"data", ",", 
     RowBox[{"ChartElementFunction", "->", 
      RowBox[{"errorBar", "[", "]"}]}], ",", "\n", 
     "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t", 
     RowBox[{"ChartLabels", "->", "dataLabels"}], ",", "\n", 
     "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t", 
     RowBox[{"ImageSize", "->", "is"}], ",", 
     RowBox[{"PlotLabel", "->", "title"}]}], "\n", 
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t", "]"}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{"kalagioAnscFractionPlotDataLabels", "=", 
   RowBox[{
    RowBox[{
     RowBox[{"(", 
      RowBox[{
       RowBox[{"ToString", "[", 
        RowBox[{"Round", "[", 
         RowBox[{
          RowBox[{"#", "[", 
           RowBox[{"[", "1", "]"}], "]"}], ",", "1"}], "]"}], "]"}], "<>", 
       "\"\<d\>\""}], ")"}], "&"}], "/@", "kalagioAnscFraction"}]}], 
  ";"}], "\n", 
 RowBox[{
  RowBox[{"kalagioActiveLrcPlotDataLabels", "=", 
   RowBox[{
    RowBox[{
     RowBox[{"(", 
      RowBox[{
       RowBox[{"ToString", "[", 
        RowBox[{"Round", "[", 
         RowBox[{
          RowBox[{"#", "[", 
           RowBox[{"[", "1", "]"}], "]"}], ",", "1"}], "]"}], "]"}], "<>", 
       "\"\<d\>\""}], ")"}], "&"}], "/@", "kalagioActiveLrc"}]}], 
  ";"}]}], "Code",
 CellChangeTimes->{{3.6947815659541693`*^9, 3.694781633299801*^9}, {
  3.6947817870075865`*^9, 3.6947817910168123`*^9}, {3.694781837146308*^9, 
  3.6947818923238616`*^9}, {3.694781944802598*^9, 3.6947819470802126`*^9}, {
  3.6947820023201666`*^9, 3.694782008653807*^9}, {3.694782071366209*^9, 
  3.694782073472223*^9}, {3.694782473335325*^9, 3.694782473460126*^9}}],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"Show", "[", 
  RowBox[{"kalagioCellTypeDeclStemPlot", ",", 
   RowBox[{"PlotLabel", "\[Rule]", 
    RowBox[{"Style", "[", 
     RowBox[{"\"\<Number of NSCs\>\"", ",", "14"}], "]"}]}]}], "]"}], "\n", 
 RowBox[{"plotD", "[", 
  RowBox[{
  "kalagioAnscFractionPlotData", ",", "kalagioAnscFractionPlotDataLabels", 
   ",", 
   RowBox[{"Style", "[", 
    RowBox[{"\"\<Fraction of aNSCs among NSCs\>\"", ",", "14"}], "]"}]}], 
  "]"}], "\n", 
 RowBox[{"plotD", "[", 
  RowBox[{
  "kalagioActiveLrcPlotData", ",", "kalagioActiveLrcPlotDataLabels", ",", 
   RowBox[{"Style", "[", 
    RowBox[{
    "\"\<Fraction of active cells among label retaining cells\>\"", ",", 
     "14"}], "]"}]}], "]"}]}], "Code",
 InitializationCell->False,
 CellChangeTimes->{{3.6947818619972672`*^9, 3.694781862090868*^9}, {
  3.694781916878419*^9, 3.694781948562222*^9}, {3.6947820529736915`*^9, 
  3.694782226992807*^9}}],

Cell[BoxData[
 GraphicsBox[{{}, 
   {RGBColor[0.24720000000000014`, 0.24, 0.6], 
    PointBox[{{60., 779.6666666666667}, {210., 273.6666666666667}, {660., 
     143.55553999999998`}}], {{
      LineBox[{{60., 856.6393934185653}, {60., 702.6939399147682}}], 
      LineBox[{Offset[{1.5, 0}, {60., 856.6393934185653}], 
        Offset[{-1.5, 0}, {60., 856.6393934185653}]}], 
      LineBox[{Offset[{1.5, 0}, {60., 702.6939399147682}], 
        Offset[{-1.5, 0}, {60., 702.6939399147682}]}]}, {
      LineBox[{{210., 331.1973896675334}, {210., 216.13594366579997`}}], 
      LineBox[{Offset[{1.5, 0}, {210., 331.1973896675334}], 
        Offset[{-1.5, 0}, {210., 331.1973896675334}]}], 
      LineBox[{Offset[{1.5, 0}, {210., 216.13594366579997`}], 
        Offset[{-1.5, 0}, {210., 216.13594366579997`}]}]}, {
      LineBox[{{660., 187.26308085834614`}, {660., 99.84799914165382}}], 
      LineBox[{Offset[{1.5, 0}, {660., 187.26308085834614`}], 
        Offset[{-1.5, 0}, {660., 187.26308085834614`}]}], 
      LineBox[{Offset[{1.5, 0}, {660., 99.84799914165382}], 
        Offset[{-1.5, 0}, {660., 99.84799914165382}]}]}}}, {}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->True,
  AxesLabel->{None, None},
  AxesOrigin->{0, 0},
  ImageSize->300,
  Method->{},
  PlotLabel->FormBox[
    StyleBox["\"Number of NSCs\"", 14, StripOnInput -> False], 
    TraditionalForm],
  PlotRangeClipping->True]], "Output",
 CellChangeTimes->{
  3.6947818546028194`*^9, {3.694781936690546*^9, 3.6947819504966345`*^9}, 
   3.694782010978222*^9, {3.6947820812254725`*^9, 3.694782227632411*^9}, 
   3.694782439054699*^9, 3.6947824745521326`*^9}],

Cell[BoxData[
 GraphicsBox[{
   {Opacity[0], PointBox[{{0.44545454545454544`, 0.}}]}, {{}, 
    {RGBColor[0.798413061722744, 0.824719615472648, 0.968322270542458], 
     EdgeForm[{Opacity[0.7], Thickness[Small]}], 
     {RGBColor[0.798413061722744, 0.824719615472648, 0.968322270542458], 
      EdgeForm[{Opacity[0.7], Thickness[Small]}], 
      TagBox[
       TooltipBox[
        TagBox[
         DynamicBox[{
           FEPrivate`If[
            CurrentValue["MouseOver"], 
            EdgeForm[{
              GrayLevel[0.5], 
              AbsoluteThickness[1.5], 
              Opacity[0.66]}], {}, {}], {
            
            RectangleBox[{0.5454545454545454, 0.}, {1.4545454545454546`, 
             0.3181818181818182}, "RoundingRadius" -> 0], {
             GrayLevel[0], 
             
             LineBox[{{{1., 0.26853051718719867`}, {1., 
              0.3678331191764377}}, {{0.7727272727272727, 
              0.3678331191764377}, {1.227272727272727, 
              0.3678331191764377}}, {{0.7727272727272727, 
              0.26853051718719867`}, {1.227272727272727, 
              0.26853051718719867`}}}]}}},
          
          ImageSizeCache->{{33.14050063099655, 
           159.19360224092145`}, {-85.41421356237312, 85.32766243484382}}],
         StatusArea[#, 0.3181818181818182]& ,
         TagBoxNote->"0.3181818181818182"],
        StyleBox["0.3181818181818182`", {
          GrayLevel[0]}, StripOnInput -> False]],
       Annotation[#, 
        Style[0.3181818181818182, {
          GrayLevel[0]}], "Tooltip"]& ]}, 
     {RGBColor[0.798413061722744, 0.824719615472648, 0.968322270542458], 
      EdgeForm[{Opacity[0.7], Thickness[Small]}], 
      TagBox[
       TooltipBox[
        TagBox[
         DynamicBox[{
           FEPrivate`If[
            CurrentValue["MouseOver"], 
            EdgeForm[{
              GrayLevel[0.5], 
              AbsoluteThickness[1.5], 
              Opacity[0.66]}], {}, {}], {
            
            RectangleBox[{1.5454545454545454`, 0.}, {2.4545454545454546`, 
             0.15151515151515152`}, "RoundingRadius" -> 0], {
             GrayLevel[0], 
             
             LineBox[{{{2., 0.12030735451245836`}, {2., 
              0.18272294851784468`}}, {{1.772727272727273, 
              0.18272294851784468`}, {2.227272727272727, 
              0.18272294851784468`}}, {{1.772727272727273, 
              0.12030735451245836`}, {2.227272727272727, 
              0.12030735451245836`}}}]}}},
          
          ImageSizeCache->{{168.68764256469308`, 
           294.74074417461793`}, {-1.4142135623731065`, 85.32766243484382}}],
         StatusArea[#, 0.15151515151515152`]& ,
         TagBoxNote->"0.15151515151515152"],
        StyleBox["0.15151515151515152`", {
          GrayLevel[0]}, StripOnInput -> False]],
       Annotation[#, 
        Style[0.15151515151515152`, {
          GrayLevel[0]}], "Tooltip"]& ]}}, {}, {}}, {}, {}, GraphicsGroupBox[
    {GrayLevel[0], 
     StyleBox[
      StyleBox[
       StyleBox[{
         {Thickness[Tiny], 
          LineBox[{{0.44545454545454544`, 0.}, {2.4545454545454546`, 
           0.}}], {}}, {
          {Thickness[Tiny], 
           LineBox[{{0.5454545454545454, 0.}, 
             Offset[{-1.102182119232618*^-15, -6.}, {0.5454545454545454, 
              0.}]}], LineBox[{{2.4545454545454546`, 0.}, 
             Offset[{-1.102182119232618*^-15, -6.}, {2.4545454545454546`, 
              0.}]}], {{}, {}, {
             LineBox[{{1.4545454545454546`, 0.}, 
               Offset[{-7.347880794884119*^-16, -4.}, {1.4545454545454546`, 
                0.}]}], 
             LineBox[{{1.5454545454545454`, 0.}, 
               Offset[{-7.347880794884119*^-16, -4.}, {1.5454545454545454`, 
                0.}]}]}, {}}}, 
          {Thickness[Tiny], 
           InsetBox["\<\"60d\"\>", Offset[{0., -2.}, {1., 0.}], {0, 1}, 
            Automatic, {1, 0}], 
           InsetBox["\<\"660d\"\>", Offset[{0., -2.}, {2., 0.}], {0, 1}, 
            Automatic, {1, 0}]}}},
        Antialiasing->False], "GraphicsAxes",
       StripOnInput->False],
      Antialiasing->False]}]},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{False, True},
  AxesLabel->{None, None},
  AxesOrigin->{0.44545454545454544`, 0},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  ImageSize->300,
  PlotLabel->FormBox[
    StyleBox["\"Fraction of aNSCs among NSCs\"", 14, StripOnInput -> False], 
    TraditionalForm],
  PlotRangePadding->Scaled[0.02],
  Ticks->{None, Automatic}]], "Output",
 CellChangeTimes->{
  3.6947818546028194`*^9, {3.694781936690546*^9, 3.6947819504966345`*^9}, 
   3.694782010978222*^9, {3.6947820812254725`*^9, 3.694782227632411*^9}, 
   3.694782439054699*^9, 3.694782474583333*^9}],

Cell[BoxData[
 GraphicsBox[{
   {Opacity[0], PointBox[{{0.44545454545454544`, 0.}}]}, {{}, 
    {RGBColor[0.798413061722744, 0.824719615472648, 0.968322270542458], 
     EdgeForm[{Opacity[0.7], Thickness[Small]}], 
     {RGBColor[0.798413061722744, 0.824719615472648, 0.968322270542458], 
      EdgeForm[{Opacity[0.7], Thickness[Small]}], 
      TagBox[
       TooltipBox[
        TagBox[
         DynamicBox[{
           FEPrivate`If[
            CurrentValue["MouseOver"], 
            EdgeForm[{
              GrayLevel[0.5], 
              AbsoluteThickness[1.5], 
              Opacity[0.66]}], {}, {}], {
            
            RectangleBox[{0.5454545454545454, 0.}, {1.4545454545454546`, 
             0.3065499619890072}, "RoundingRadius" -> 0], {
             GrayLevel[0], 
             
             LineBox[{{{1., 0.29113693088508874`}, {1., 
              0.32196299309292564`}}, {{0.7727272727272727, 
              0.32196299309292564`}, {1.227272727272727, 
              0.32196299309292564`}}, {{0.7727272727272727, 
              0.29113693088508874`}, {1.227272727272727, 
              0.29113693088508874`}}}]}}},
          
          ImageSizeCache->{{28.625061362098208`, 
           113.62871507294712`}, {-86.4142135623731, 85.12165110526055}}],
         StatusArea[#, 0.3065499619890072]& ,
         TagBoxNote->"0.3065499619890072"],
        StyleBox["0.3065499619890072`", {
          GrayLevel[0]}, StripOnInput -> False]],
       Annotation[#, 
        Style[0.3065499619890072, {
          GrayLevel[0]}], "Tooltip"]& ]}, 
     {RGBColor[0.798413061722744, 0.824719615472648, 0.968322270542458], 
      EdgeForm[{Opacity[0.7], Thickness[Small]}], 
      TagBox[
       TooltipBox[
        TagBox[
         DynamicBox[{
           FEPrivate`If[
            CurrentValue["MouseOver"], 
            EdgeForm[{
              GrayLevel[0.5], 
              AbsoluteThickness[1.5], 
              Opacity[0.66]}], {}, {}], {
            
            RectangleBox[{1.5454545454545454`, 0.}, {2.4545454545454546`, 
             0.22184804284017834`}, "RoundingRadius" -> 0], {
             GrayLevel[0], 
             
             LineBox[{{{2., 0.20093203751815877`}, {2., 
              0.2427640481621979}}, {{1.772727272727273, 
              0.2427640481621979}, {2.227272727272727, 
              0.2427640481621979}}, {{1.772727272727273, 
              0.20093203751815877`}, {2.227272727272727, 
              0.20093203751815877`}}}]}}},
          
          ImageSizeCache->{{119.0178106068112, 
           204.02146431766013`}, {-44.41421356237311, 85.12165110526058}}],
         StatusArea[#, 0.22184804284017834`]& ,
         TagBoxNote->"0.22184804284017834"],
        StyleBox["0.22184804284017834`", {
          GrayLevel[0]}, StripOnInput -> False]],
       Annotation[#, 
        Style[0.22184804284017834`, {
          GrayLevel[0]}], "Tooltip"]& ]}, 
     {RGBColor[0.798413061722744, 0.824719615472648, 0.968322270542458], 
      EdgeForm[{Opacity[0.7], Thickness[Small]}], 
      TagBox[
       TooltipBox[
        TagBox[
         DynamicBox[{
           FEPrivate`If[
            CurrentValue["MouseOver"], 
            EdgeForm[{
              GrayLevel[0.5], 
              AbsoluteThickness[1.5], 
              Opacity[0.66]}], {}, {}], {
            
            RectangleBox[{2.5454545454545454`, 0.}, {3.4545454545454546`, 
             0.20048809831418532`}, "RoundingRadius" -> 0], {
             GrayLevel[0], 
             
             LineBox[{{{3., 0.19390154907085705`}, {3., 
              0.2070746475575136}}, {{2.772727272727273, 
              0.2070746475575136}, {3.227272727272727, 
              0.2070746475575136}}, {{2.772727272727273, 
              0.19390154907085705`}, {3.227272727272727, 
              0.19390154907085705`}}}]}}},
          
          ImageSizeCache->{{209.4105598515242, 
           294.41421356237316`}, {-25.414213562373106`, 85.12165110526058}}],
         StatusArea[#, 0.20048809831418532`]& ,
         TagBoxNote->"0.20048809831418532"],
        StyleBox["0.20048809831418532`", {
          GrayLevel[0]}, StripOnInput -> False]],
       Annotation[#, 
        Style[0.20048809831418532`, {
          GrayLevel[0]}], "Tooltip"]& ]}}, {}, {}}, {}, {}, GraphicsGroupBox[
    {GrayLevel[0], 
     StyleBox[
      StyleBox[
       StyleBox[{
         {Thickness[Tiny], 
          LineBox[{{0.44545454545454544`, 0.}, {3.4545454545454546`, 
           0.}}], {}}, {
          {Thickness[Tiny], 
           LineBox[{{0.5454545454545454, 0.}, 
             Offset[{-1.102182119232618*^-15, -6.}, {0.5454545454545454, 
              0.}]}], LineBox[{{3.4545454545454546`, 0.}, 
             Offset[{-1.102182119232618*^-15, -6.}, {3.4545454545454546`, 
              0.}]}], {{}, {}, {}, {
             LineBox[{{1.4545454545454546`, 0.}, 
               Offset[{-7.347880794884119*^-16, -4.}, {1.4545454545454546`, 
                0.}]}], 
             LineBox[{{1.5454545454545454`, 0.}, 
               Offset[{-7.347880794884119*^-16, -4.}, {1.5454545454545454`, 
                0.}]}], 
             LineBox[{{2.4545454545454546`, 0.}, 
               Offset[{-7.347880794884119*^-16, -4.}, {2.4545454545454546`, 
                0.}]}], 
             LineBox[{{2.5454545454545454`, 0.}, 
               Offset[{-7.347880794884119*^-16, -4.}, {2.5454545454545454`, 
                0.}]}]}, {}}}, 
          {Thickness[Tiny], 
           InsetBox["\<\"60d\"\>", Offset[{0., -2.}, {1., 0.}], {0, 1}, 
            Automatic, {1, 0}], 
           InsetBox["\<\"210d\"\>", Offset[{0., -2.}, {2., 0.}], {0, 1}, 
            Automatic, {1, 0}], 
           InsetBox["\<\"660d\"\>", Offset[{0., -2.}, {3., 0.}], {0, 1}, 
            Automatic, {1, 0}]}}},
        Antialiasing->False], "GraphicsAxes",
       StripOnInput->False],
      Antialiasing->False]}]},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{False, True},
  AxesLabel->{None, None},
  AxesOrigin->{0.44545454545454544`, 0},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  ImageSize->300,
  PlotLabel->FormBox[
    StyleBox[
    "\"Fraction of active cells among label retaining cells\"", 14, 
     StripOnInput -> False], TraditionalForm],
  PlotRangePadding->Scaled[0.02],
  Ticks->{None, Automatic}]], "Output",
 CellChangeTimes->{
  3.6947818546028194`*^9, {3.694781936690546*^9, 3.6947819504966345`*^9}, 
   3.694782010978222*^9, {3.6947820812254725`*^9, 3.694782227632411*^9}, 
   3.694782439054699*^9, 3.6947824745989327`*^9}]
}, Open  ]]
},
WindowSize->{1904, 1065},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
FrontEndVersion->"9.0 for Microsoft Windows (64-bit) (January 25, 2013)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[557, 20, 2043, 53, 233, "Code"],
Cell[CellGroupData[{
Cell[2625, 77, 919, 23, 93, "Code",
 InitializationCell->False],
Cell[3547, 102, 1650, 33, 219, "Output"],
Cell[5200, 137, 4780, 113, 218, "Output"],
Cell[9983, 252, 6568, 155, 218, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
