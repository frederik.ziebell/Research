(* ::Package:: *)

(* ::Program:: *)
(*Data import, source: Capilla-Gonzalez, 2014, Glia*)


(*==================================================================================================
Type: Number of B, C, A, B1 and BrdU+ cells in the SVZ during aging
Protocol: electron-microscopy of the SVZ niche, BrdU once 50mg/kg and sac. after 2h
Structure: list with elements {age [d], mean, sem}
==================================================================================================*)
capillaCellTypeDeclB1=Import["data/capilla2014_cellTypeDecl.xlsx"][[4,2;;]];
capillaCellTypeDeclC=Import["data/capilla2014_cellTypeDecl.xlsx"][[2,2;;]];
capillaCellTypeDeclA=Import["data/capilla2014_cellTypeDecl.xlsx"][[3,2;;]];
capillaTotalBrdu=Import["data/capilla2014_cellTypeDecl.xlsx"][[5,2;;]];


(* ::Program:: *)
(*Data plotting*)


Import["code/plotting.m"];
(*======================================================================================================================
CellTypeDecl
======================================================================================================================*)
capillaCellTypeDeclB1PlotData=(#[[2]]->#[[3]])&/@capillaCellTypeDeclB1;
capillaCellTypeDeclB1PlotDataLabels=(ToString[Round[#[[1]],1]]<>"d\ndata")&/@capillaCellTypeDeclB1;
capillaCellTypeDeclB1PlotModelLabels=(ToString[Round[#[[1]],1]]<>"d\nmodel")&/@capillaCellTypeDeclB1;
capillaCellTypeDeclCPlotData=(#[[2]]->#[[3]])&/@capillaCellTypeDeclC;
capillaCellTypeDeclCPlotDataLabels=(ToString[Round[#[[1]],1]]<>"d\ndata")&/@capillaCellTypeDeclC;
capillaCellTypeDeclCPlotModelLabels=(ToString[Round[#[[1]],1]]<>"d\nmodel")&/@capillaCellTypeDeclC;
capillaCellTypeDeclAPlotData=(#[[2]]->#[[3]])&/@capillaCellTypeDeclA;
capillaCellTypeDeclAPlotDataLabels=(ToString[Round[#[[1]],1]]<>"d\ndata")&/@capillaCellTypeDeclA;
capillaCellTypeDeclAPlotModelLabels=(ToString[Round[#[[1]],1]]<>"d\nmodel")&/@capillaCellTypeDeclA;

(*======================================================================================================================
TotalBrdu
======================================================================================================================*)
capillaTotalBrduPlotData=(#[[2]]->#[[3]])&/@capillaTotalBrdu;
capillaTotalBrduPlotDataLabels=(ToString[Round[#[[1]],1]]<>"d\ndata")&/@capillaTotalBrdu;
capillaTotalBrduPlotModelLabels=(ToString[Round[#[[1]],1]]<>"d\nmodel")&/@capillaTotalBrdu;


(* ::Program:: *)
(*Data modeling*)


(*==================================================================================================
Type: Number of B1, C, A cells in the SVZ during aging
Protocol: electron-microscopy of the SVZ niche
==================================================================================================*)
(* number of B1, C and A cells at particular age *)
capillaCellTypeDeclB1CAFct[r_,b_,d_,f_,nStem0_,age_]:=Module[{init,cells},
	init=nStem0*UnitVector[n+6,1];
	(* number of existing cells at the given age *)
	cells=Through[neuroModel[r,b,d,f,0,age,init][age]];
	(* number of {NSCs, progenitors, nblasts} *)
	Return[{cells[[1]]+cells[[2]],Total[cells[[3;;3+n]]],Total[cells[[4+n;;5+n]]]}];
];
capillaCellTypeDeclB1Fct[r_,b_,d_,f_,nStem0_,age_]:=capillaCellTypeDeclB1CAFct[r,b,d,f,nStem0,age][[1]];
capillaCellTypeDeclCFct[r_,b_,d_,f_,nStem0_,age_]:=capillaCellTypeDeclB1CAFct[r,b,d,f,nStem0,age][[2]];
capillaCellTypeDeclAFct[r_,b_,d_,f_,nStem0_,age_]:=capillaCellTypeDeclB1CAFct[r,b,d,f,nStem0,age][[3]];

(* total number of BrdU positive stem and progenitor cells *)
capillaTotalBrduFct[r_,b_,d_,f_,nStem0_,age_]:=Module[{\[Delta]2h,init,cells,cellsBrdu,cellsBrduPos},
	\[Delta]2h=2/24; (* all time must be in the unit days *)
	init=nStem0*UnitVector[n+6,1];
	(* number of existing cells at the given age *)
	cells=Through[neuroModel[r,b,d,f,0,age,init][age]];
	(* number of BrdU labeled cells *)
	cellsBrdu=getLabeledCells[cells,\[Delta]Brdu50];
	(* number of BrdU labeled cells 2h after BrdU *)
	cellsBrduPos=Through[neuroModel[r,b,d,f,age,age+\[Delta]2h,cellsBrdu][age+\[Delta]2h]];
	(* number of BrdU positive stem cells and progenitors *)
	Return[Total[cells[[1;;3+n]]]];
];
