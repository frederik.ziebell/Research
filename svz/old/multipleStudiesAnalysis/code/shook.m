(* ::Package:: *)

(* ::Program:: *)
(*Data import, source: Shook, 2013, J Neuroscience*)


(*==================================================================================================
Type: Number of stem (GFAP+) cells during aging
Protocol: GFAP staining after sac.
Structure: list with elements {age [d], mean, sem}
==================================================================================================*)
shookNscDecl=Import["data/shook2013_nscDecl.xlsx"][[1,2;;]];

(*==================================================================================================
Type: Neuron (EdU+ OB cells) addition during aging
Protocol: 3x daily 150mg/kg EdU, sac. 3w after EdU 
Protocol assumption: "3x daily" = 3 shots 8h apart, "3w after EdU" = 3w after last injection
Structure: list with elements {age [d], mean, sem}
==================================================================================================*)
shookNeuroAdd=Import["data/shook2013_neuroAdd.xlsx"][[1,2;;]];


(* ::Program:: *)
(*Data plotting*)


Import["code/plotting.m"];
(*======================================================================================================================
NscDecl
======================================================================================================================*)
shookNscDeclPlot=ErrorBarPlots`ErrorListPlot[
	{{#[[1]],#[[2]]},ErrorBarPlots`ErrorBar[#[[3]]]}&/@shookNscDecl,
	PlotRange->All,AxesOrigin->{0,0},ImageSize->is
];

(*======================================================================================================================
NeuroAdd
======================================================================================================================*)
shookNeuroAddPlotData=(#[[2]]->#[[3]])&/@shookNeuroAdd;
shookNeuroAddPlotDataLabels=(ToString[Round[#[[1]],1]]<>"d\ndata")&/@shookNeuroAdd;
shookNeuroAddPlotModelLabels=(ToString[Round[#[[1]],1]]<>"d\nmodel")&/@shookNeuroAdd;


(* ::Program:: *)
(*Data modeling*)


(*======================================================================================================================
Type: Number of stem (GFAP+) cells during aging
Protocol: GFAP staining after sac.
======================================================================================================================*)
(* total number of stem cells *)
shookNscDeclFct[r_,b_,d_,f_,nStem0_,age_]:=Module[{init,cells},
	init=nStem0*UnitVector[n+6,1];
	(* number of existing cells at the given age *)
	cells=Through[neuroModel[r,b,d,f,0,age,init][age]];
	Return[cells[[1]]+cells[[2]]];
];

(*==================================================================================================
Type: Neuron (EdU+ OB cells) addition during aging
Protocol: 3x daily 150mg/kg EdU, sac. 3w after EdU 
Protocol assumption: "3x daily" = 3 shots 8h apart, "3w after EdU" = 3w after last injection
Modeling assumption: EdU decays like BrdU, although molar masses are different
==================================================================================================*)
(* number of BrdU positive neurons *)
shookNeuroAddFct[r_,b_,d_,f_,nStem0_,age_]:=Module[{age1stShot,age2ndShot,age3rdShot,age3w,init,cells1stExist,cells1stEduPos,cells1stEduNeg,cells2ndExist,cells2ndEduPos,cells2ndEduNeg,cells3rdExist,cells3rdEduPos,cells3wEduPos},
	(* age at time of 1st, 2nd and 3rd EdU shot *)
	age1stShot=age;
	age2ndShot=age+8/24; (* age must be in the unit days *)
	age3rdShot=age2ndShot+8/24; (* age2ndShot must be in the unit days *)
	age3w=age3rdShot+21; (* age3rdShot must be in the unit days *)
	(* initial data at hypothetical age=0 *)
	init=nStem0*UnitVector[n+6,1];
	(* 1st shot *)
		(* number of existing cells at time of 1st EdU injection *)
		cells1stExist=Through[neuroModel[r,b,d,f,0,age1stShot,init][age1stShot]];
		(* number of cells getting EdU positive by 1st shot *)
		cells1stEduPos=getLabeledCells[cells1stExist,\[Delta]Brdu150];
		(* number of cells not getting EdU positive by 1st shot *)
		cells1stEduNeg=cells1stExist-cells1stEduPos;
	(* 2nd shot *)
		(* number of existing Edu negative cells at time of 2nd injection *)
		cells2ndExist=Through[neuroModel[r,b,d,f,age1stShot,age2ndShot,cells1stEduNeg][age2ndShot]];
		(* number of cells getting EdU positive by 2nd shot *)
		cells2ndEduPos=getLabeledCells[cells2ndExist,\[Delta]Brdu150];
		(* number of cells not getting EdU positive by 2nd shot *)
		cells2ndEduNeg=cells2ndExist-cells2ndEduPos;
	(* 3rd shot *)
		(* number of existing EdU negative cells at time of 3rd injection *)
		cells3rdExist=Through[neuroModel[r,b,d,f,age2ndShot,age3rdShot,cells2ndEduNeg][age3rdShot]];
		(* number of cells getting EdU positive by 3rd shot *)
		cells3rdEduPos=getLabeledCells[cells3rdExist,\[Delta]Brdu150];
	(* Number of EdU positive cells 3w after 3rd shot *)
	cells3wEduPos=(Through[neuroModel[r,b,d,f,age1stShot,age3w,cells1stEduPos][age3w]]
					+Through[neuroModel[r,b,d,f,age2ndShot,age3w,cells2ndEduPos][age3w]]
					+Through[neuroModel[r,b,d,f,age3rdShot,age3w,cells3rdEduPos][age3w]]);
	(* return number of EdU labeled neurons *)
	Return[cells3wEduPos[[-1]]];
];
