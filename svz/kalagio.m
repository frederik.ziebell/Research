(* ::Package:: *)

(* ::Program:: *)
(*Data import, source: Georgios Kalamakis*)


(*======================================================================================================================
Type: Number of qNSCs, aNSCs and progenitors during aging
Protocol: FACS sorting for qNSCs (GFAP+CD133+EGFR-CD24-), aNSCs (GFAP+CD133+EGFR+CD24-) and
		  TACs (GFAP-CD133-EGFR+CD24-)
Structure: {age [d], mean number of cells, sem}
======================================================================================================================*)
kalagioCellTypeDeclQnsc=Flatten[Partition[Riffle[#[[2;;]],#[[1]],{1,-2,2}],2]&/@DeleteCases[Import["data/kalagio.xlsx"][[1]],"",{2}][[2;;]],1];
kalagioCellTypeDeclQnsc={#[[1]],Mean[#[[2]]],StandardDeviation[#[[2]]]/Sqrt[Length[#[[2]]]]}&/@Table[{x,Select[kalagioCellTypeDeclQnsc,#[[1]]==x&][[;;,2]]},{x,Union[kalagioCellTypeDeclQnsc[[;;,1]]]}];
kalagioCellTypeDeclAnsc=Flatten[Partition[Riffle[#[[2;;]],#[[1]],{1,-2,2}],2]&/@DeleteCases[Import["data/kalagio.xlsx"][[2]],"",{2}][[2;;]],1];
kalagioCellTypeDeclAnsc={#[[1]],Mean[#[[2]]],StandardDeviation[#[[2]]]/Sqrt[Length[#[[2]]]]}&/@Table[{x,Select[kalagioCellTypeDeclAnsc,#[[1]]==x&][[;;,2]]},{x,Union[kalagioCellTypeDeclAnsc[[;;,1]]]}];
kalagioCellTypeDeclProg=Flatten[Partition[Riffle[#[[2;;]],#[[1]],{1,-2,2}],2]&/@DeleteCases[Import["data/kalagio.xlsx"][[3]],"",{2}][[2;;]],1];
kalagioCellTypeDeclProg={#[[1]],Mean[#[[2]]],StandardDeviation[#[[2]]]/Sqrt[Length[#[[2]]]]}&/@Table[{x,Select[kalagioCellTypeDeclProg,#[[1]]==x&][[;;,2]]},{x,Union[kalagioCellTypeDeclProg[[;;,1]]]}];
kalagioCellTypeDeclStem=kalagioCellTypeDeclQnsc;
kalagioCellTypeDeclStem[[;;,2;;]]=kalagioCellTypeDeclQnsc[[;;,2;;]]+kalagioCellTypeDeclAnsc[[;;,2;;]];

(*======================================================================================================================
Type: Fraction of aNSCs on all NSCs from small number of RNA-sequenced qNSC1s, qNSC2s and aNSCs during aging
Protocol: Cell isolation followed by RNAseq
Structure: {age [d], fraction of aNSCs on all NSCs, standard deviation}
Note: Since the fraction is the binomial proportion p, the standard deviation is the standard 
deviation of X/n where X ~ B(n,p).
======================================================================================================================*)
kalagioAnscFraction={#[[1]],#[[4]]/Total[#[[2;;4]]],Sqrt[(#[[4]]/Total[#[[2;;4]]]*(1-#[[4]]/Total[#[[2;;4]]]))/Total[#[[2;;4]]]]}&/@Import["data/kalagio.xlsx"][[4,2;;]];

(*======================================================================================================================
Type: Active label retaining fraction
Protocol: BrdU administration for 2w in the drinking water, analysis 2w after BrdU stop
Structure: {age [d], mean fraction of active label retaining cells on all label retaining cells, sem}
======================================================================================================================*)
kalagioActiveLrc=Import["data/kalagio.xlsx"][[5,2;;]];
kalagioActiveLrc={#[[1,1]],Total[#[[;;,4]]]/Total[#[[;;,3;;4]],2]}&/@GatherBy[kalagioActiveLrc,#[[1;;2]]&];
kalagioActiveLrc={#[[1]],Mean[#[[2]]],StandardDeviation[#[[2]]]/Sqrt[Length[#[[2]]]]}&/@Table[{x,Select[kalagioActiveLrc,#[[1]]==x&][[;;,2]]},{x,Union[kalagioActiveLrc[[;;,1]]]}];

(*======================================================================================================================
Type: Active label retaining fraction
Protocol: BrdU administration for 2w in the drinking water, treatment with (Cxcl10 or IgG) 2w after BrdU stop for 1w, 
          analysis after inhibition stop
Structure: {treatment, age [d], mean fraction of active label retaining cells on all label retaining cells, sem}
======================================================================================================================*)
kalagioCxc=Import["data/kalagio.xlsx"][[6,2;;]];
kalagioCxc={#[[1,2]],#[[1,1]],#[[;;,4]]/(#[[;;,3]]+#[[;;,4]])}&/@GatherBy[kalagioCxc,#[[1;;2]]&];
kalagioCxc={#[[1]],#[[2]],Mean[#[[3]]],StandardDeviation[#[[3]]]/Sqrt[Length[#[[3]]]]}&/@kalagioCxc;

(*======================================================================================================================
Type: Active label retaining fraction
Protocol: BrdU administration for 2w in the drinking water, treatment with (PBS or Foxy5) 2w after BrdU stop for 1w, 
          analysis after inhibition stop
Structure: {treatment, age [d], mean fraction of active label retaining cells on all label retaining cells, sem}
======================================================================================================================*)
kalagioSfr=Import["data/kalagio.xlsx"][[7,2;;]];
kalagioSfr={#[[1,2]],#[[1,1]],#[[;;,4]]/(#[[;;,3]]+#[[;;,4]])}&/@GatherBy[kalagioSfr,#[[1;;2]]&];
kalagioSfr={#[[1]],#[[2]],Mean[#[[3]]],StandardDeviation[#[[3]]]/Sqrt[Length[#[[3]]]]}&/@kalagioSfr;


(* ::Program:: *)
(*Data plotting*)


Import["plotting.m"];
(*======================================================================================================================
CellTypeDecl
======================================================================================================================*)
kalagioCellTypeDeclQnscPlot=ErrorBarPlots`ErrorListPlot[
	{{#[[1]],#[[2]]},ErrorBarPlots`ErrorBar[#[[3]]]}&/@kalagioCellTypeDeclQnsc,
	PlotStyle->Directive[Black,Thick],
	PlotRange->All,AxesOrigin->{0,0},ImageSize->is
];
kalagioCellTypeDeclAnscPlot=ErrorBarPlots`ErrorListPlot[
	{{#[[1]],#[[2]]},ErrorBarPlots`ErrorBar[#[[3]]]}&/@kalagioCellTypeDeclAnsc,
	PlotStyle->Directive[Black,Thick],
	PlotRange->All,AxesOrigin->{0,0},ImageSize->is
];
kalagioCellTypeDeclStemPlot=ErrorBarPlots`ErrorListPlot[
	{{#[[1]],#[[2]]},ErrorBarPlots`ErrorBar[#[[3]]]}&/@kalagioCellTypeDeclStem,
	PlotStyle->Directive[Black,Thick],
	PlotRange->All,AxesOrigin->{0,0},ImageSize->is
];
kalagioCellTypeDeclProgPlot=ErrorBarPlots`ErrorListPlot[
	{{#[[1]],#[[2]]},ErrorBarPlots`ErrorBar[#[[3]]]}&/@kalagioCellTypeDeclProg,
	PlotStyle->Directive[Black,Thick],
	PlotRange->All,AxesOrigin->{0,0},ImageSize->is
];
(*======================================================================================================================
AnscFraction
======================================================================================================================*)
kalagioAnscFractionPlotData=(#[[2]]->#[[3]])&/@kalagioAnscFraction;
kalagioAnscFractionPlotDataLabels=(ToString[Round[#[[1]],1]]<>"d\ndata")&/@kalagioAnscFraction;
kalagioAnscFractionPlotModelLabels=(ToString[Round[#[[1]],1]]<>"d\nmodel")&/@kalagioAnscFraction;
(*======================================================================================================================
ActiveLrc
======================================================================================================================*)
kalagioActiveLrcPlotData=(#[[2]]->#[[3]])&/@kalagioActiveLrc;
kalagioActiveLrcPlotDataLabels=(ToString[Round[#[[1]],1]]<>"d\ndata")&/@kalagioActiveLrc;
kalagioActiveLrcPlotModelLabels=(ToString[Round[#[[1]],1]]<>"d\nmodel")&/@kalagioActiveLrc;
(*======================================================================================================================
Cxc
======================================================================================================================*)
kalagioCxcPlotData=(#[[3]]->#[[4]])&/@kalagioCxc;
kalagioCxcPlotDataLabels=(#[[1]]<>"\ndata")&/@kalagioCxc;
kalagioCxcPlotModelLabels=(#[[1]]<>"\nmodel")&/@kalagioCxc;
(*======================================================================================================================
Sfr
======================================================================================================================*)
kalagioSfrPlotData=(#[[3]]->#[[4]])&/@kalagioSfr;
kalagioSfrPlotDataLabels=(#[[1]]<>"\ndata")&/@kalagioSfr;
kalagioSfrPlotModelLabels=(#[[1]]<>"\nmodel")&/@kalagioSfr;
