(* ::Package:: *)

(* ::Program:: *)
(*Data import, source: Codega, Neuron, 2014*)


(*==================================================================================================
Type: Fraction of BrdU+ quiescent (GFAP+CD133+) and active (GFAP+CD133+EGFR+)
Protocol: BrdU application followed by FACS sorting at two time points after BrdU
Structure: list with elements {age [d], time of FACS analysis after BrdU [d], mean, sem}
==================================================================================================*)
codegaStemQuiBrdu=Import["data/codega2014_neuroProlif.xlsx"][[1,2;;]];
codegaStemActBrdu=Import["data/codega2014_neuroProlif.xlsx"][[2,2;;]];


(* ::Program:: *)
(*Data plotting*)


Import["code/plotting.m"];
(*======================================================================================================================
StemBrdu
======================================================================================================================*)
codegaStemQuiBrduPlotData=(#[[3]]->#[[4]])&/@codegaStemQuiBrdu;
codegaStemQuiBrduPlotDataLabels=(ToString[Round[#[[2]]*24,1]]<>"h after,\nBrdU data")&/@codegaStemQuiBrdu;
codegaStemQuiBrduPlotModelLabels=(ToString[Round[#[[2]]*24,1]]<>"h after,\nBrdU model")&/@codegaStemQuiBrdu;
codegaStemActBrduPlotData=(#[[3]]->#[[4]])&/@codegaStemActBrdu;
codegaStemActBrduPlotDataLabels=(ToString[Round[#[[2]]*24,1]]<>"h after,\nBrdU data")&/@codegaStemActBrdu;
codegaStemActBrduPlotModelLabels=(ToString[Round[#[[2]]*24,1]]<>"h after,\nBrdU model")&/@codegaStemActBrdu;


(* ::Program:: *)
(*Data modeling*)


(*==================================================================================================
Type: Fraction of BrdU+ quiescent (GFAP+CD133+) and active (GFAP+CD133+EGFR+)
Protocol: BrdU application followed by FACS sorting at two time points after BrdU
==================================================================================================*)
(* fraction of BrdU pos quiescent and active stem cells among BrdU pos cells *)
codegaStemBrduFct[r_,b_,d_,f_,nStem0_,age_,dt_]:=Module[{\[Delta]2h,init,cells,cellsBrdu,cellsBrduPos,totalBrdu},
	init=nStem0*UnitVector[n+6,1];
	(* number of existing cells at the given age *)
	cells=Through[neuroModel[r,b,d,f,0,age,init][age]];
	(* number of BrdU labeled cells *)
	cellsBrdu=getLabeledCells[cells,\[Delta]Brdu50];
	(* number of BrdU labeled cells dt time units after BrdU *)
	cellsBrduPos=Through[neuroModel[r,b,d,f,age,age+dt,cellsBrdu][age+dt]];
	(* number of measured BrdU positive cells = BrdU positive stem cellls and progenitors *)
	totalBrdu=Total[cellsBrduPos[[1;;3+n]]];
	Return[cellsBrduPos[[1;;2]]/totalBrdu];
];
codegaStemQuiBrduFct[r_,b_,d_,f_,nStem0_,age_,dt_]:=codegaStemBrduFct[r,b,d,f,nStem0,age,dt][[1]];
codegaStemActBrduFct[r_,b_,d_,f_,nStem0_,age_,dt_]:=codegaStemBrduFct[r,b,d,f,nStem0,age,dt][[2]];
