(* ::Package:: *)

(* ::Program:: *)
(*Data import, source: Shook, 2013, J Neuroscience*)


(*==================================================================================================
Source: Silva-Vargas, 2016, Cell Stem Cell
Type: Number of qNSCs, aNSCs and progenitors during aging
Protocol: FACS sorting for qNSCs (GFAP+CD133+EGFR-CD24-), aNSCs (GFAP+CD133+EGFR+CD24-) and
		  TACs (GFAP-CD133-EGFR+CD24-)
Structure: {age [d], mean number of cells, sem}
==================================================================================================*)
silvaCellTypeDeclQnsc=Flatten[Partition[Riffle[#[[2;;]],#[[1]],{1,-2,2}],2]&/@Import["data/silvaVargas2016_cellTypeDecl.xlsx"][[1,2;;]],1];
silvaCellTypeDeclQnsc={#[[1]],Mean[#[[2]]],StandardDeviation[#[[2]]]/Sqrt[Length[#[[2]]]]}&/@Table[{x,Select[silvaCellTypeDeclQnsc,#[[1]]==x&][[;;,2]]},{x,Union[silvaCellTypeDeclQnsc[[;;,1]]]}];
silvaCellTypeDeclAnsc=Flatten[Partition[Riffle[#[[2;;]],#[[1]],{1,-2,2}],2]&/@Import["data/silvaVargas2016_cellTypeDecl.xlsx"][[2,2;;]],1];
silvaCellTypeDeclAnsc={#[[1]],Mean[#[[2]]],StandardDeviation[#[[2]]]/Sqrt[Length[#[[2]]]]}&/@Table[{x,Select[silvaCellTypeDeclAnsc,#[[1]]==x&][[;;,2]]},{x,Union[silvaCellTypeDeclAnsc[[;;,1]]]}];
silvaCellTypeDeclProg=Flatten[Partition[Riffle[#[[2;;]],#[[1]],{1,-2,2}],2]&/@Import["data/silvaVargas2016_cellTypeDecl.xlsx"][[3,2;;]],1];
silvaCellTypeDeclProg={#[[1]],Mean[#[[2]]],StandardDeviation[#[[2]]]/Sqrt[Length[#[[2]]]]}&/@Table[{x,Select[silvaCellTypeDeclProg,#[[1]]==x&][[;;,2]]},{x,Union[silvaCellTypeDeclProg[[;;,1]]]}];


(* ::Program:: *)
(*Data plotting*)


Import["code/plotting.m"];
(*======================================================================================================================
CellTypeDecl
======================================================================================================================*)
silvaCellTypeDeclQnscPlotData=(#[[2]]->#[[3]])&/@silvaCellTypeDeclQnsc;
silvaCellTypeDeclQnscPlotDataLabels=(ToString[Round[#[[1]],1]]<>"d\ndata")&/@silvaCellTypeDeclQnsc;
silvaCellTypeDeclQnscPlotModelLabels=(ToString[Round[#[[1]],1]]<>"d\nmodel")&/@silvaCellTypeDeclQnsc;
silvaCellTypeDeclAnscPlotData=(#[[2]]->#[[3]])&/@silvaCellTypeDeclAnsc;
silvaCellTypeDeclAnscPlotDataLabels=(ToString[Round[#[[1]],1]]<>"d\ndata")&/@silvaCellTypeDeclAnsc;
silvaCellTypeDeclAnscPlotModelLabels=(ToString[Round[#[[1]],1]]<>"d\nmodel")&/@silvaCellTypeDeclAnsc;
silvaCellTypeDeclProgPlotData=(#[[2]]->#[[3]])&/@silvaCellTypeDeclProg;
silvaCellTypeDeclProgPlotDataLabels=(ToString[Round[#[[1]],1]]<>"d\ndata")&/@silvaCellTypeDeclProg;
silvaCellTypeDeclProgPlotModelLabels=(ToString[Round[#[[1]],1]]<>"d\nmodel")&/@silvaCellTypeDeclProg;


(* ::Program:: *)
(*Data modeling*)


(*==================================================================================================
Type: Number of qNSCs, aNSCs and progenitors during aging
Protocol: FACS sorting for qNSCs (GFAP+CD133+EGFR-CD24-), qNSCs (GFAP+CD133+EGFR+CD24-) and
		  TACs (GFAP-CD133-EGFR+CD24-)
==================================================================================================*)
(* number of qNSCs, aNSCs and progenitors at particular age *)
silvaCellTypeDeclFct[r_,b_,d_,f_,nStem0_,age_]:=Module[{init,cells},
	init=nStem0*UnitVector[n+6,1];
	(* number of existing cells at the given age *)
	cells=Through[neuroModel[r,b,d,f,0,age,init][age]];
	(* number of {qNSCs, aNSCs, progenitors} *)
	Return[{cells[[1]],cells[[2]],Total[cells[[3;;3+n]]]}];
];
silvaCellTypeDeclQnscFct[r_,b_,d_,f_,nStem0_,age_]:=silvaCellTypeDeclFct[r,b,d,f,nStem0,age][[1]];
silvaCellTypeDeclAnscFct[r_,b_,d_,f_,nStem0_,age_]:=silvaCellTypeDeclFct[r,b,d,f,nStem0,age][[2]];
silvaCellTypeDeclProgFct[r_,b_,d_,f_,nStem0_,age_]:=silvaCellTypeDeclFct[r,b,d,f,nStem0,age][[3]];
