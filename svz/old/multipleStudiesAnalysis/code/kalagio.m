(* ::Package:: *)

(* ::Program:: *)
(*Data import, source: Georgios Kalamakis*)


(*SetDirectory[ParentDirectory[NotebookDirectory[]]];*)
(*======================================================================================================================
Type: Number of qNSCs, aNSCs and progenitors during aging
Protocol: FACS sorting for qNSCs (GFAP+CD133+EGFR-CD24-), aNSCs (GFAP+CD133+EGFR+CD24-) and
		  TACs (GFAP-CD133-EGFR+CD24-)
Structure: {age [d], mean number of cells, sem}
======================================================================================================================*)
kalagioCellTypeDeclQnsc=Flatten[Partition[Riffle[#[[2;;]],#[[1]],{1,-2,2}],2]&/@DeleteCases[Import["data/kalagio.xlsx"][[1]],"",{2}][[2;;]],1];
kalagioCellTypeDeclQnsc={#[[1]],Mean[#[[2]]],StandardDeviation[#[[2]]]/Sqrt[Length[#[[2]]]]}&/@Table[{x,Select[kalagioCellTypeDeclQnsc,#[[1]]==x&][[;;,2]]},{x,Union[kalagioCellTypeDeclQnsc[[;;,1]]]}];
kalagioCellTypeDeclAnsc=Flatten[Partition[Riffle[#[[2;;]],#[[1]],{1,-2,2}],2]&/@DeleteCases[Import["data/kalagio.xlsx"][[2]],"",{2}][[2;;]],1];
kalagioCellTypeDeclAnsc={#[[1]],Mean[#[[2]]],StandardDeviation[#[[2]]]/Sqrt[Length[#[[2]]]]}&/@Table[{x,Select[kalagioCellTypeDeclAnsc,#[[1]]==x&][[;;,2]]},{x,Union[kalagioCellTypeDeclAnsc[[;;,1]]]}];
kalagioCellTypeDeclProg=Flatten[Partition[Riffle[#[[2;;]],#[[1]],{1,-2,2}],2]&/@DeleteCases[Import["data/kalagio.xlsx"][[3]],"",{2}][[2;;]],1];
kalagioCellTypeDeclProg={#[[1]],Mean[#[[2]]],StandardDeviation[#[[2]]]/Sqrt[Length[#[[2]]]]}&/@Table[{x,Select[kalagioCellTypeDeclProg,#[[1]]==x&][[;;,2]]},{x,Union[kalagioCellTypeDeclProg[[;;,1]]]}];
kalagioCellTypeDeclStem=kalagioCellTypeDeclQnsc;
kalagioCellTypeDeclStem[[;;,2;;]]=kalagioCellTypeDeclQnsc[[;;,2;;]]+kalagioCellTypeDeclAnsc[[;;,2;;]];

(*======================================================================================================================
Type: Fraction of aNSCs on all NSCs from small number of RNA-sequenced qNSC1s, qNSC2s and aNSCs during aging
Protocol: Cell isolation followed by RNAseq
Structure: {age [d], fraction of aNSCs on all NSCs, standard deviation}
Note: Since the fraction is the binomial proportion p, the standard deviation is the standard 
deviation of X/n where X ~ B(n,p).
======================================================================================================================*)
kalagioAnscFraction={#[[1]],#[[4]]/Total[#[[2;;4]]],Sqrt[(#[[4]]/Total[#[[2;;4]]]*(1-#[[4]]/Total[#[[2;;4]]]))/Total[#[[2;;4]]]]}&/@Import["data/kalagio.xlsx"][[4,2;;]];

(*======================================================================================================================
Type: Active label retaining fraction
Protocol: BrdU administration for 2w in the drinking water, analysis 2w after BrdU stop
Structure: {age [d], mean fraction of active label retaining cells on all label retaining cells, sem}
======================================================================================================================*)
kalagioActiveLrc=Import["data/kalagio.xlsx"][[5,2;;]];
kalagioActiveLrc={#[[1,1]],Total[#[[;;,4]]]/Total[#[[;;,3;;4]],2]}&/@GatherBy[kalagioActiveLrc,#[[1;;2]]&];
kalagioActiveLrc={#[[1]],Mean[#[[2]]],StandardDeviation[#[[2]]]/Sqrt[Length[#[[2]]]]}&/@Table[{x,Select[kalagioActiveLrc,#[[1]]==x&][[;;,2]]},{x,Union[kalagioActiveLrc[[;;,1]]]}];


(* ::Program:: *)
(*Data plotting*)


Import["code/plotting.m"];
(*======================================================================================================================
CellTypeDecl
======================================================================================================================*)
kalagioCellTypeDeclQnscPlot=ErrorBarPlots`ErrorListPlot[
	{{#[[1]],#[[2]]},ErrorBarPlots`ErrorBar[#[[3]]]}&/@kalagioCellTypeDeclQnsc,
	PlotRange->All,AxesOrigin->{0,0},ImageSize->is
];
kalagioCellTypeDeclAnscPlot=ErrorBarPlots`ErrorListPlot[
	{{#[[1]],#[[2]]},ErrorBarPlots`ErrorBar[#[[3]]]}&/@kalagioCellTypeDeclAnsc,
	PlotRange->All,AxesOrigin->{0,0},ImageSize->is
];
kalagioCellTypeDeclStemPlot=ErrorBarPlots`ErrorListPlot[
	{{#[[1]],#[[2]]},ErrorBarPlots`ErrorBar[#[[3]]]}&/@kalagioCellTypeDeclStem,
	PlotRange->All,AxesOrigin->{0,0},ImageSize->is
];
kalagioCellTypeDeclProgPlot=ErrorBarPlots`ErrorListPlot[
	{{#[[1]],#[[2]]},ErrorBarPlots`ErrorBar[#[[3]]]}&/@kalagioCellTypeDeclProg,
	PlotRange->All,AxesOrigin->{0,0},ImageSize->is
];
(*======================================================================================================================
AnscFraction
======================================================================================================================*)
kalagioAnscFractionPlotData=(#[[2]]->#[[3]])&/@kalagioAnscFraction;
kalagioAnscFractionPlotDataLabels=(ToString[Round[#[[1]],1]]<>"d\ndata")&/@kalagioAnscFraction;
kalagioAnscFractionPlotModelLabels=(ToString[Round[#[[1]],1]]<>"d\nmodel")&/@kalagioAnscFraction;
(*======================================================================================================================
ActiveLrc
======================================================================================================================*)
kalagioActiveLrcPlotData=(#[[2]]->#[[3]])&/@kalagioActiveLrc;
kalagioActiveLrcPlotDataLabels=(ToString[Round[#[[1]],1]]<>"d\ndata")&/@kalagioActiveLrc;
kalagioActiveLrcPlotModelLabels=(ToString[Round[#[[1]],1]]<>"d\nmodel")&/@kalagioActiveLrc;


(* ::Program:: *)
(*Data modeling*)


(*======================================================================================================================
Type: Number of qNSCs, aNSCs and progenitors during aging
Protocol: FACS sorting for qNSCs (GFAP+CD133+EGFR-CD24-), aNSCs (GFAP+CD133+EGFR+CD24-) and
		  TACs (GFAP-CD133-EGFR+CD24-)
======================================================================================================================*)
(* number of qNSCs, aNSCs and progenitors at particular age *)
kalagioCellTypeDeclFct[r_,b_,d_,f_,nStem0_,age_]:=Module[{init,cells},
	init=nStem0*UnitVector[n+6,1];
	(* number of existing cells at the given age *)
	cells=Through[neuroModel[r,b,d,f,0,age,init][age]];
	(* number of {qNSCs, aNSCs, progenitors} *)
	Return[{cells[[1]],cells[[2]],Total[cells[[3;;3+n]]]}];
];
kalagioCellTypeDeclQnscFct[r_,b_,d_,f_,nStem0_,age_]:=kalagioCellTypeDeclFct[r,b,d,f,nStem0,age][[1]];
kalagioCellTypeDeclAnscFct[r_,b_,d_,f_,nStem0_,age_]:=kalagioCellTypeDeclFct[r,b,d,f,nStem0,age][[2]];
kalagioCellTypeDeclProgFct[r_,b_,d_,f_,nStem0_,age_]:=kalagioCellTypeDeclFct[r,b,d,f,nStem0,age][[3]];
kalagioCellTypeDeclStemFct[r_,b_,d_,f_,nStem0_,age_]:=kalagioCellTypeDeclFct[r,b,d,f,nStem0,age][[1]]+kalagioCellTypeDeclFct[r,b,d,f,nStem0,age][[2]];

(*======================================================================================================================
Type: Fraction of aNSCs on all NSCs from small number of RNA-sequenced qNSC1s, qNSC2s and aNSCs during aging
Protocol: Cell isolation followed by RNAseq
======================================================================================================================*)
kalagioAnscFractionFct[r_,b_,d_,f_,nStem0_,age_]:=kalagioCellTypeDeclFct[r,b,d,f,nStem0,age][[2]]/(kalagioCellTypeDeclFct[r,b,d,f,nStem0,age][[1]]+kalagioCellTypeDeclFct[r,b,d,f,nStem0,age][[2]]);

(*======================================================================================================================
Type: Active label retaining fraction
Protocol: BrdU administration for 2w in the drinking water, analysis 2w after BrdU stop
======================================================================================================================*)
kalagioActiveLrcFct[r_,b_,d_,f_,nStem0_,age_,\[Rho]Ki67_]:=Module[{},
	

];
